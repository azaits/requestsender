ENV['RACK_ENV'] = 'test' 

files = %w(rubygems sinatra pry eventmachine em-websocket sinatra/base thin sequel sqlite3 json net/http haml)
files.each{|file| require file}

require "rack/test"

require "capybara"
require 'capybara/cucumber'

require './db/schema.rb'
require './link.rb'
require './request_sender.rb'
require './app.rb'

app_file = File.join(File.dirname(__FILE__), *%w[.. .. app.rb])

# Force the application name because polyglot breaks the auto-detection logic.
Sinatra::Application.app_file = app_file
Capybara.app = App


class MyWorld
 
  include Rack::Test::Methods
  include Capybara::DSL
  include RSpec::Matchers
  
  def app
    App
  end
end

World{MyWorld.new}