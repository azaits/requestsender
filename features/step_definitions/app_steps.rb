Given /^I am viewing "(.*?)"$/ do |url|
  visit(url)
end

Then /^I should see "(.+)"$/ do |text|
  page.body.should =~ Regexp.new(Regexp.escape(text))
end
