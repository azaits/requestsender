class Link < Sequel::Model

  dataset_module do

    SUCCESS = 'success'
    FAILED = 'failed'
    PROCESSING = 'processing'

    STATUSES = [SUCCESS, FAILED, PROCESSING]

    STATUSES.each do |status|
      define_method status do
        filter(status: status)
      end
    end

    def Link.processing_results
      STATUSES.inject({}) {|res, state| res[state] = Link.send(state).count; res}
    end

  end

end