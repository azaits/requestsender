@db = Sequel.connect('sqlite://links.db')

@db.create_table :links do
  primary_key :id
  string      :url
  string      :status, default: 'processing'
  integer     :attempts_count, default: 0
  index       :status
end rescue nil