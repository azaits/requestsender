files = %w(rubygems sinatra pry eventmachine em-websocket sinatra/base thin sequel sqlite3 json em-http haml)
files.each{|e| require e}

require './db/schema.rb'
require './link.rb'
require './request_sender.rb'
require "./app.rb"

RequestSender.run App.new
