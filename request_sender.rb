module RequestSender

  def self.run(app)
    EventMachine.run do

      @dispatch = Rack::Builder.app do
        map '/' do
          run app
        end
      end

      EventMachine::WebSocket.start(host: "0.0.0.0", port: 8081) do |ws|
        ws.onopen do 
           EventMachine.add_periodic_timer(1) do
            ws.send(Link.processing_results.to_json)
          end 
        end
      end

      @link_processor = Proc.new do
        links = Link.processing
        STDOUT.puts "processing: #{links.count} links ..."
        links.each do |link|

          STDOUT.puts "attempt: #{link.attempts_count} processing: #{link.url}"

          begin
            http = EventMachine::HttpRequest.new(URI.parse(link.url)).get
          rescue Addressable::URI::InvalidURIError
            link.update_only({:status => Link::FAILED}, :status); break
          end

          http.errback do 
            mark_as_failed(link)
          end

          http.callback do 
            if http.response_header.status == 200
              link.update_only({:status => Link::SUCCESS}, :status) 
            else  
              mark_as_failed(link)
            end
          end
             
        end
      end

      EventMachine.add_periodic_timer(5, &@link_processor)

      Rack::Server.start({
        app:    @dispatch,
        server: 'thin',
        host:   '0.0.0.0',
        port:   '8080'
      })

    end
  end

  private

    def self.mark_as_failed(link)
      link.update_only({:attempts_count => link.attempts_count += 1}, :attempts_count)
      link.update_only({:status => Link::FAILED}, :status) if link.attempts_count == 5
    end

end
