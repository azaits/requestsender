class App < Sinatra::Base

  get '/send' do
    if params[:url]
      EventMachine.defer do
        puts "adding link: #{params[:url]}"
        Link.create(url: params[:url])
      end
      { status: 'OK' }.to_json
    end
  end

  get '/' do
    haml :index
  end

end

