require File.expand_path '../spec_helper.rb', __FILE__

describe Link do

  before :each do 
    Link.truncate
    @link_processing = Link.create(url: 'http://facebook.com/')
    @link_success = Link.create(url: 'http://vk.com/', status: 'success')
    @link_failed = Link.create(url: 'http://twitter.com/', status: 'failed')
  end

  
  it "should have constatnts defined" do
    Link.constants.should include(:SUCCESS, :FAILED, :PROCESSING, :STATUSES)
  end

  it "should have methods defined" do
    Link.methods.should include(:success, :failed, :processing, :processing_results)
  end

  it "should be processing by default" do
    @link_processing.status.should == 'processing'
  end

  it "should return links with state" do
    Link.processing.should include(@link_processing)
    Link.failed.should include(@link_failed)
    Link.success.should include(@link_success)
  end

  it "should return current processing state" do
    Link.processing_results.should == {"success" => 1, "failed" => 1, "processing" => 1}
  end

end