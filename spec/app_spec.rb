require File.expand_path '../spec_helper.rb', __FILE__

describe "Sinatra Application" do

  it "should allow accessing the stats page" do
    get '/'
    last_response.should be_ok
  end

  it "should render the stats page template" do
    get '/'
    last_response.body.should match(/Links Processor/)
    last_response.body.should match(/Add url to process/)
    last_response.body.should match(/App Log/)
  end

  it "should be possible to add link to processing" do
    get '/send', :url => 'http://facebook.com'
    last_response.should be_ok
  end

  it "should render json response" do
    get '/send', :url => 'http://facebook.com'
    last_response.body.should == {status:'OK'}.to_json
  end

  it "should not start processing without url" do
    get '/send'
    last_response.body.should == ''
  end

end