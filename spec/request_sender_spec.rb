require File.expand_path '../spec_helper.rb', __FILE__

describe RequestSender do

  before do 
    Link.truncate
  end

  def run_link_processor
    link_processor = RequestSender.instance_variable_get(:@link_processor)
    EventMachine.run_block {link_processor.call} 
  end

  it 'executes specs within a reactor loop' do
    EM.reactor_running?.should be_true 
  end

  it 'should run application, web socket, timer and server' do 
    Rack::Builder.should_receive(:app).with()
    EventMachine::WebSocket.should_receive(:start)
    EventMachine.should_receive(:add_periodic_timer).with(5)
    Rack::Server.should_receive(:start)
    RequestSender.run(App.new)
  end

  it 'should not attempt call with bad url and mark link as failed' do 
    bad_link = Link.create(url: 'bad_link')
    stub_request(:get, "http://facebook.com/").to_return(:status => 200, :body => "", :headers => {})
    run_link_processor
    bad_link.refresh
    bad_link.status.should == 'failed'
  end

  it 'should simulate connection timeout and mark as failed after 5 attempts' do 
    link = Link.create(url: 'http://facebook.com/', :attempts_count => 4)
    stub_request(:get, link.url).to_timeout
    run_link_processor
    # TODO find way to refresh DB
    # link.status.should == 'failed'
  end

  it 'should mark link as success' do 
    link = Link.create(url: 'http://facebook.com/')
    stub_request(:any, link.url).to_return(:status => 200)
    run_link_processor
    # TODO find way to refresh DB
    # link.status.should == 'success'
  end

  it 'should mark link as failed after 5 attempts' do 
    link = Link.create(url: 'http://example.com/', :attempts_count => 4)
    stub_request(:any, link.url).to_return(:status => 500 )
    run_link_processor
    # TODO find way to refresh DB
    # link.status.should == 'failed'
  end

end