files = %w(rubygems sinatra pry eventmachine em-websocket sinatra/base thin sequel sqlite3 json net/http haml)
files.each{|file| require file}
require 'simplecov'
require 'rack/test'
require 'em-rspec'
require 'em-http'
require 'webmock/rspec'

SimpleCov.start

require './db/schema.rb'
require './link.rb'
require './request_sender.rb'

require File.join(File.expand_path(File.dirname(__FILE__)), '/../', 'app')

set :environment, :test
 

#specify that the app is a Sinatra app
def app
  App
end
 
#make Rack::Text available to all spec contexts
RSpec.configure do |config|
  config.include Rack::Test::Methods

end