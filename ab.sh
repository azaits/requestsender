#!/bin/bash

ab -n 100 -c 10 http://localhost:8080/ > index.log &
ab -n 100 -c 10 http://localhost:8080/send?url=http://stackoverflow.com/ > processing.log &
